# Friendship Bracelets

The aim of this software is providing a tool for all the swifties worldwide to create their friendship bracelets.

## What is in here?
In this repository you can find the executable files for the software. You just need to download the one which is suitable for your computer: bracelets_ubuntu.sh for ubuntu users, bracelets_windows.exe for Windows users and bracelets_macOS for mac users.

## How to use it?
1. Collect all your letters in the first column of an Excel file. You can create a different file for each type of letters that you have (i.e. one file for black and white letters and another for black and gold letters).
2. Open the program by double clicking on it.
3. Load the excel file of the letters by clicking on "Load excel file".
4. Analyze the suggestions given by the software.
5. Select the words that you like the most and check if they are feasible to make with the functionality "Check combination".
6. Check any other idea that you have by writing on the blank space ;) next to "Check word".
7. Finally type the words that you will create in the blank space next to "Finalize and Export letters" without any separation between them and click on the button to update the list of letters remaining.


